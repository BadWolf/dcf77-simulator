# DCF77 simulator

For development of the Lixie clock, it was helpful to have a device that could emulate an ideal DCF77 signal.  
This is just a small Arduino sketch that can be loaded onto a second device to be plugged into the circuit instead of the actual DCF77 antenna.  
It plays a pre-defined set of valid time data taken from [dcf77logs](https://dcf77logs.de).
